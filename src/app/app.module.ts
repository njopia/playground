import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AngularSharedModule } from './shared/material-shared.module';
import { SharedModule } from './shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { SideNavComponent } from './shuttle/sidenav/sidenav/sidenav.component';

@NgModule({
    declarations: [AppComponent, SideNavComponent],
    providers: [],
    bootstrap: [AppComponent],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        AngularSharedModule,
        SharedModule,
        ReactiveFormsModule,

    ]
})
export class AppModule {}
