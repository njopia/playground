import { MediaMatcher } from '@angular/cdk/layout';
import { OverlayContainer } from '@angular/cdk/overlay';
import {
  ChangeDetectorRef,
  Component,
  HostBinding,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { FormControl } from '@angular/forms';

const nav = [
  {
    routerLink: '/',
    title: 'Home',
    icon: 'home',
    label: 'Home',
  },
  {
    routerLink: 'material',
    title: 'Shows Angular material components',
    icon: 'home',
    label: 'Material Components',
  },
  {
    routerLink: '/material/draganddrop',
    title: 'Shows Angular material components',
    icon: 'home',
    label: 'Drag & Drop',
  },
];
@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css'],
})
export class SideNavComponent implements OnDestroy, OnInit {
  public ARRAY_NAV_ITEMS: any[] = nav;
  mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;
  toogleControl = new FormControl(true);
  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private overlay: OverlayContainer
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }
  darkClassName = 'theme-dark';
  lightClassName = 'theme-light';
  @HostBinding('class') className = this.darkClassName;

  ngOnInit(): void {
    this.toogleControl.valueChanges.subscribe((darkMode) => {
      this.className = darkMode ? this.darkClassName : this.lightClassName;
      if (darkMode) {
        this.overlay.getContainerElement().classList.add(this.darkClassName);
      } else {
        this.overlay.getContainerElement().classList.remove(this.darkClassName);
      }
    });
  }
  fillerContent = Array.from(
    {length: 50},
    () =>
      `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
       labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
       laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
       voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
       cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`,
  );


  title = 'Angular - PLAYGROUND';
  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }
}
